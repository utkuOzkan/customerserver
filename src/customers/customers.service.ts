import { Injectable, Body, UsePipes, Post, ValidationPipe, NotFoundException } from '@nestjs/common';
import { CreateCustomerDto } from './dto/create-customer.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { CustomerRepository } from './customer.repository';
import { Customer } from './customer.entity';
import { GetCustomerFilterDto } from './dto/get-customer-filter';

@Injectable()
export class CustomersService {
    constructor(
        @InjectRepository(CustomerRepository)
        private customerRepository: CustomerRepository,
    ) { }

    async getAllCustomers(
        filterDto: GetCustomerFilterDto
    ): Promise<[Customer[],number]> {
        return this.customerRepository.getCustomers(filterDto);
    }


    async getCustomerById(id: number): Promise<Customer> {
        const found = await this.customerRepository.findOne(id);

        if (!found) {
            throw new NotFoundException(`Customer with ID "${id}" not found`);
        }
        return found;
    }

    async createCustomer(
        createCustomerDto: CreateCustomerDto
    ): Promise<Customer> {

        const { fName, lName, email, gender, phone } = createCustomerDto;

        const customer = new Customer();
        customer.first_name = fName;
        customer.last_name = lName;
        customer.email = email;
        customer.gender = gender;
        customer.phone = phone;

        await customer.save();

        return customer;
    }

    async deleteTask(id: number): Promise<void> {
        const result = await this.customerRepository.delete(id);

        if (result.affected === 0) {
            throw new NotFoundException(`Customer with ID "${id}" not found`);
        }
    }


    async updateCustomer(id: number, createCustomerDto: Partial<CreateCustomerDto>) {
        const customer = await this.getCustomerById(id);
        const { fName, lName, email, gender, phone } = createCustomerDto;

        customer.first_name = fName;
        customer.last_name = lName;
        customer.email = email;
        customer.gender = gender;
        customer.phone = phone;

        await customer.save();
        return customer;
    }
}
