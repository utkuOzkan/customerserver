import {IsNotEmpty, IsEmail} from 'class-validator'
import { GenderStatus } from '../customer-status.enum';

export class CreateCustomerDto{
    
    @IsNotEmpty()
    fName: string;
    @IsNotEmpty()
    lName: string;
    @IsNotEmpty()
    @IsEmail()
    email: string;
    @IsNotEmpty()
    gender: GenderStatus;
    @IsNotEmpty()
    phone: string;
}