import { IsOptional, IsIn, IsNotEmpty } from "class-validator";

export class GetCustomerFilterDto {
    @IsOptional()
    @IsNotEmpty()
    search: string;

    @IsNotEmpty()
    page: number;

    @IsNotEmpty()
    pageSize: number;

}