import { Customer } from "./customer.entity";
import { EntityRepository, Repository } from "typeorm";
import { GetCustomerFilterDto } from "./dto/get-customer-filter";
import { Logger } from "@nestjs/common";


@EntityRepository(Customer)
export class CustomerRepository extends Repository<Customer>{

    private logger = new Logger();

    async getCustomers(
        filterDto: GetCustomerFilterDto
    ): Promise<[Customer[],number]> {
        const { search, page, pageSize } = filterDto;
        const query = this.createQueryBuilder('customer')

        if (search)
            query.andWhere('customer.last_name LIKE :search OR customer.first_name LIKE :search', { search: `%${search}%` });

        query.offset(page * pageSize)
            .limit(pageSize);

        try {
            const customer = await query.getManyAndCount();;
            return customer;
            
        } catch (error) {
            this.logger.error(`Failed to get customers. Filters: ${JSON.stringify(filterDto)}`, error.stack)
        }
    }

}