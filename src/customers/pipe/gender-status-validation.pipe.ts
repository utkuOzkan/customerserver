import { PipeTransform, BadRequestException } from "@nestjs/common";
import { GenderStatus } from "../customer-status.enum";


export class GenderStatusValidationPipe implements PipeTransform {

    readonly allowedStatuses = [
        GenderStatus.MALE,
        GenderStatus.FEMALE,
    ];

    transform(value: any) {
        console.log(value);
        // value = value.toUpperCase();
        if (!this.isStatusValid(value)) {
            throw new BadRequestException(`"${value}" is an invalid status`);
        }
        return value;
    }

    private isStatusValid(status: any) {
        const idx = this.allowedStatuses.indexOf(status.toString());
        
        return idx !== -1;
    }


}