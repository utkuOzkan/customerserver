import { Controller, Get, Param, ParseIntPipe, Delete, Patch, Put, Query } from '@nestjs/common';
import { CustomersService } from './customers.service';
import { Body, UsePipes, Post, ValidationPipe } from '@nestjs/common';
import { CreateCustomerDto } from './dto/create-customer.dto';
import { Customer } from './customer.entity';
import { GetCustomerFilterDto } from './dto/get-customer-filter';

@Controller('customers')
export class CustomersController {

    constructor(private customersService: CustomersService) { }

    @Get()
    getAllCustomers(
        @Query(ValidationPipe) filterDto: GetCustomerFilterDto
    ): Promise<[Customer[],number]> {
        return this.customersService.getAllCustomers(filterDto);
    }

    @Get('/:id')
    getCustomerById(@Param('id', ParseIntPipe) id: number): Promise<Customer> {
        return this.customersService.getCustomerById(id);
    }

    @Post()
    @UsePipes(ValidationPipe)
    createCustomer(
        @Body() createCustomerDto: CreateCustomerDto
    ): Promise<Customer> {
        console.log(createCustomerDto)
        return this.customersService.createCustomer(createCustomerDto);
    }
    
    @Delete('/:id')
     deleteCustomer(
        @Param('id', ParseIntPipe) id: number,
    ): Promise<void> {
        return this.customersService.deleteTask(id);
    }
    
    @Put(':id')
    update(@Param('id') id: number, @Body() createCustomerDto: CreateCustomerDto) {
        console.log(createCustomerDto);

        return this.customersService.updateCustomer(id, createCustomerDto);
    }

}
