import { BaseEntity, Entity, PrimaryGeneratedColumn, Column, ManyToOne, CreateDateColumn } from "typeorm";
import { GenderStatus } from "./customer-status.enum";


@Entity()
export class Customer extends BaseEntity {
        @PrimaryGeneratedColumn()
        id: number;
        @Column()
        first_name: string;
        @Column()
        last_name: string;
        @Column()
        email: string;
        @Column()
        gender: GenderStatus;
        @Column()
        phone: string;
        @CreateDateColumn()
        created: Date;
}
