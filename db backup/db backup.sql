--
-- PostgreSQL database dump
--

-- Dumped from database version 11.4
-- Dumped by pg_dump version 11.4

-- Started on 2019-07-27 22:36:20

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 197 (class 1259 OID 24764)
-- Name: customer; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.customer (
    id integer NOT NULL,
    first_name character varying NOT NULL,
    last_name character varying NOT NULL,
    email character varying NOT NULL,
    gender character varying NOT NULL,
    phone character varying NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.customer OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 24762)
-- Name: customer_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.customer_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.customer_id_seq OWNER TO postgres;

--
-- TOC entry 2818 (class 0 OID 0)
-- Dependencies: 196
-- Name: customer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.customer_id_seq OWNED BY public.customer.id;


--
-- TOC entry 2686 (class 2604 OID 24767)
-- Name: customer id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.customer ALTER COLUMN id SET DEFAULT nextval('public.customer_id_seq'::regclass);


--
-- TOC entry 2812 (class 0 OID 24764)
-- Dependencies: 197
-- Data for Name: customer; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.customer (id, first_name, last_name, email, gender, phone, created) FROM stdin;
1	Linet	Pervoe	lpervoe0@webeden.co.uk	MALE	(824) 4816647	2019-07-27 22:13:00.581842
2	Algernon	Jachtym	ajachtym1@noaa.gov	MALE	(861) 5329703	2019-07-27 22:16:47.118682
3	Jolene	Manton	jmanton2@salon.com	FEMALE	(924) 1471866	2019-07-27 22:17:39.535857
4	Whitney	Paulig	wpaulig3@usnews.com	MALE	(428) 7107463	2019-07-27 22:19:23.919372
5	Elita	Shambrook	eshambrook4@jigsy.com	Female	(801) 5946028	2019-07-27 22:20:25.728769
6	Rudie	Wynes	rwynes5@delicious.com	Male	(752) 8320773	2019-07-27 22:21:11.877938
7	Nealson	Hanner	nhannere@mediafire.com	MALE	(338) 1377222	2019-07-27 22:23:26.877774
8	Georas	Water	gwatero@ted.com	MALE	(996) 5065156	2019-07-27 22:24:21.668328
9	Kathi	Gresham	kgreshamp@wired.com	FEMALE	(650) 6478757	2019-07-27 22:25:03.194226
10	Cassaundra	Tubridy	ctubridyt@instagram.com	FEMALE	(955) 8434958	2019-07-27 22:25:52.872229
11	Deanne	Groome	dgroomev@surveymonkey.com	FEMALE	(446) 3016598	2019-07-27 22:26:37.739179
\.


--
-- TOC entry 2819 (class 0 OID 0)
-- Dependencies: 196
-- Name: customer_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.customer_id_seq', 11, true);


--
-- TOC entry 2689 (class 2606 OID 24773)
-- Name: customer PK_a7a13f4cacb744524e44dfdad32; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.customer
    ADD CONSTRAINT "PK_a7a13f4cacb744524e44dfdad32" PRIMARY KEY (id);


-- Completed on 2019-07-27 22:36:20

--
-- PostgreSQL database dump complete
--

